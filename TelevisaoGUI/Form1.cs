﻿using System;
using System.Windows.Forms;

namespace TelevisaoGUI
{
    public partial class Form1 : Form
    {
        private Tv minhaTv;
        
        public Form1()
        {
            InitializeComponent();
            minhaTv = new Tv();
        }

        private void btnOnOff_Click(object sender, EventArgs e)
        {
            if (minhaTv.Estado)
            {
                btnOnOff.Text = "Ligar Tv";
                minhaTv.DesligaTv();
                lblCanal.Text = string.Empty;
                lblVolume.Text = string.Empty;
                btnCanalMais.Enabled = false;
                btnCanalMenos.Enabled = false;
                btnVolumeMais.Enabled = false;
                btnVolumeMenos.Enabled = false;
            }
            else
            {
                btnOnOff.Text = "Desligar Tv";
                minhaTv.LigaTv();
                lblCanal.Text = minhaTv.Canal.ToString();
                lblVolume.Text = minhaTv.Volume.ToString();
                btnCanalMais.Enabled = true;
                btnCanalMenos.Enabled = true;
                btnVolumeMais.Enabled = true;
                btnVolumeMenos.Enabled = true;
            }

            lblStatus.Text = minhaTv.Mensagem;
        }

        private void btnCanalMenos_Click(object sender, EventArgs e)
        {
            minhaTv.Canal -=1;
            lblCanal.Text = minhaTv.Canal.ToString();
        }

        private void btnCanalMais_Click(object sender, EventArgs e)
        {
            minhaTv.Canal += 1;
            lblCanal.Text = minhaTv.Canal.ToString();
        }

        private void btnVolumeMenos_Click(object sender, EventArgs e)
        {
            minhaTv.Volume -=1;
            lblVolume.Text = minhaTv.Volume.ToString();
        }

        private void btnVolumeMais_Click(object sender, EventArgs e)
        {
            minhaTv.Volume += 1;
            lblVolume.Text = minhaTv.Volume.ToString();
        }

    }
}
