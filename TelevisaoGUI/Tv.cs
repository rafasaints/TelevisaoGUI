﻿namespace TelevisaoGUI
{
    using System;
    using System.IO;

    public class Tv
    {

        #region Atributos

        private bool estado;

        private int canal;

        private string mensagem;

        private int volume; 

        #endregion

        #region Propriedades

        public bool Estado { get; private set; }

        public int Canal
        {
            get { return canal; }

            set
            {
                if (value >= 0 && value <= 250)
                {
                    canal = value;
                }
            }
        }

        public int Volume
        {
            get { return volume; }

            set
            {
                if (value >= 0 && value <= 100)
                {
                    volume = value;
                }
            }
        }

        public string Mensagem { get ; private set; }

        #endregion


        #region Construtores
        public Tv()
        {
            Estado = false;
            Canal = 1;
            Volume = 50;
            Mensagem = "Nova tv criada com sucesso!";
        }

        #endregion


        public void LigaTv()
        {
            if (!Estado)
            {
                Estado = true;
                LerInfo();
                Mensagem = "TV Ligada!";
            }
        }

        public void DesligaTv()
        {
            if (Estado)
            {
                Estado = false;
                GravarInfo();
                Mensagem = "TV Desligada!";
            }
        }

        private void GravarInfo()
        {
            string ficheiro = @"tvInfo.txt";

            string linha = canal + ";" + volume;

            StreamWriter sw = new StreamWriter(ficheiro, false);

            if (!File.Exists(ficheiro))
            {
                sw = File.CreateText(ficheiro);
            }

            sw.WriteLine(linha);
            sw.Close();
        }

        private void LerInfo()
        {
            string ficheiro = @"tvInfo.txt";

            StreamReader sr;

            if (File.Exists(ficheiro))
            {
                sr = File.OpenText(ficheiro);

                string linha = "";

                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = new string[2];

                    campos = linha.Split(';');

                    canal = Convert.ToInt32(campos[0]);
                    volume = Convert.ToInt32(campos[1]);
                }
                sr.Close();
            }
        }
    }
}
