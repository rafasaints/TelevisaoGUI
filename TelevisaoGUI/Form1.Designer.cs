﻿namespace TelevisaoGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOnOff = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnCanalMais = new System.Windows.Forms.Button();
            this.btnCanalMenos = new System.Windows.Forms.Button();
            this.lblCanal = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblVolume = new System.Windows.Forms.Label();
            this.btnVolumeMais = new System.Windows.Forms.Button();
            this.btnVolumeMenos = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOnOff
            // 
            this.btnOnOff.Location = new System.Drawing.Point(469, 196);
            this.btnOnOff.Name = "btnOnOff";
            this.btnOnOff.Size = new System.Drawing.Size(108, 96);
            this.btnOnOff.TabIndex = 0;
            this.btnOnOff.Text = "Ligar Tv";
            this.btnOnOff.UseVisualStyleBackColor = true;
            this.btnOnOff.Click += new System.EventHandler(this.btnOnOff_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(26, 196);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(94, 17);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Tv Desligada!";
            // 
            // btnCanalMais
            // 
            this.btnCanalMais.Enabled = false;
            this.btnCanalMais.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCanalMais.Location = new System.Drawing.Point(135, 33);
            this.btnCanalMais.Name = "btnCanalMais";
            this.btnCanalMais.Size = new System.Drawing.Size(48, 39);
            this.btnCanalMais.TabIndex = 2;
            this.btnCanalMais.Text = "+";
            this.btnCanalMais.UseVisualStyleBackColor = true;
            this.btnCanalMais.Click += new System.EventHandler(this.btnCanalMais_Click);
            // 
            // btnCanalMenos
            // 
            this.btnCanalMenos.Enabled = false;
            this.btnCanalMenos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCanalMenos.Location = new System.Drawing.Point(16, 33);
            this.btnCanalMenos.Name = "btnCanalMenos";
            this.btnCanalMenos.Size = new System.Drawing.Size(48, 39);
            this.btnCanalMenos.TabIndex = 3;
            this.btnCanalMenos.Text = "-";
            this.btnCanalMenos.UseVisualStyleBackColor = true;
            this.btnCanalMenos.Click += new System.EventHandler(this.btnCanalMenos_Click);
            // 
            // lblCanal
            // 
            this.lblCanal.AutoSize = true;
            this.lblCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCanal.Location = new System.Drawing.Point(90, 42);
            this.lblCanal.Name = "lblCanal";
            this.lblCanal.Size = new System.Drawing.Size(16, 20);
            this.lblCanal.TabIndex = 5;
            this.lblCanal.Text = "-";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCanalMenos);
            this.groupBox1.Controls.Add(this.lblCanal);
            this.groupBox1.Controls.Add(this.btnCanalMais);
            this.groupBox1.Location = new System.Drawing.Point(39, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 92);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Canal";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblVolume);
            this.groupBox2.Controls.Add(this.btnVolumeMais);
            this.groupBox2.Controls.Add(this.btnVolumeMenos);
            this.groupBox2.Location = new System.Drawing.Point(323, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 92);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Volume";
            // 
            // lblVolume
            // 
            this.lblVolume.AutoSize = true;
            this.lblVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolume.Location = new System.Drawing.Point(97, 42);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(16, 20);
            this.lblVolume.TabIndex = 6;
            this.lblVolume.Text = "-";
            // 
            // btnVolumeMais
            // 
            this.btnVolumeMais.Enabled = false;
            this.btnVolumeMais.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolumeMais.Location = new System.Drawing.Point(146, 33);
            this.btnVolumeMais.Name = "btnVolumeMais";
            this.btnVolumeMais.Size = new System.Drawing.Size(48, 39);
            this.btnVolumeMais.TabIndex = 6;
            this.btnVolumeMais.Text = "+";
            this.btnVolumeMais.UseVisualStyleBackColor = true;
            this.btnVolumeMais.Click += new System.EventHandler(this.btnVolumeMais_Click);
            // 
            // btnVolumeMenos
            // 
            this.btnVolumeMenos.Enabled = false;
            this.btnVolumeMenos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolumeMenos.Location = new System.Drawing.Point(16, 33);
            this.btnVolumeMenos.Name = "btnVolumeMenos";
            this.btnVolumeMenos.Size = new System.Drawing.Size(48, 39);
            this.btnVolumeMenos.TabIndex = 6;
            this.btnVolumeMenos.Text = "-";
            this.btnVolumeMenos.UseVisualStyleBackColor = true;
            this.btnVolumeMenos.Click += new System.EventHandler(this.btnVolumeMenos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 304);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnOnOff);
            this.Name = "Form1";
            this.Text = "Televisão";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOnOff;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnCanalMais;
        private System.Windows.Forms.Button btnCanalMenos;
        private System.Windows.Forms.Label lblCanal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblVolume;
        private System.Windows.Forms.Button btnVolumeMais;
        private System.Windows.Forms.Button btnVolumeMenos;
    }
}

